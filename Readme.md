# Official Mobile Identity Git workflow description for iOS FF project

**Table of contents:**

* [Purpose](#markdown-header-purpose)
* [Main development line](#markdown-header-main-development-line)
* [Feature branch workflow](#markdown-header-feature-branch-workflow)
* [Branch naming conventions](#markdown-header-branch-naming-conventions)
* [Pull requests](#markdown-header-pull-requests)
* [Other notes](#markdown-header-other-notes)
* [Submodules **NOT**](#markdown-header-say-no-to-submodules)


***
# Purpose
This is is a preliminary document describing _Git workflow_ which must be used while working on FF project. It is heavily inspired by _Atlassian_ excellent article on [Feature Branch development](
https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow) mostly copying from it shamelessly and adding specific details on branch naming conventions and extra branching patterns to handle FF client / partner application clones.

```
This is a 1st draft and your constructive input and improvement suggestions are very much welcomed. 
However, until the changes are agreed upon and document is updated, 
please follow its instructions and treat as final.
```

## Main development line


Dev branch serves as a trunk branch and represents the main development line. Never work directly on `dev` branch nor merge into it without changes being reviewed. Whenever you work on new feature, bugfix or stuff related to the FF partner clone, always create a new branch and follow both: [workflow](#markdown-header-feature-branch-workflow) and [branch naming conventions](#markdown-header-branch-naming-conventions) described below.

## Feature Branch Workflow

The core idea behind the *Feature Branch Workflow* is that all feature development should take place in a dedicated branch instead of the `dev` branch. This encapsulation makes it easy for multiple developers to work on a particular feature without disturbing the main codebase. It also means the de branch will never contain broken code, which is a huge advantage for continuous integration environments.

Encapsulating feature development also makes it possible to leverage pull requests, which are a way to initiate discussions around a branch. They give other developers the opportunity to sign off on a feature before it gets integrated into the official project. Or, if you get stuck in the middle of a feature, you can open a pull request asking for suggestions from your colleagues. The point is, pull requests make it incredibly easy for your team to comment on each other’s work.

**How It Works**

As stated already: `dev` branch represents the official project history. Developers newer work on it direclty but create a new branch every time they start work on a new feature. Feature branches should have descriptive names and be grouped into categories (more on that later), f.ex:

 - `feature/101-animated-menu-items`
 - `bugfix/102-unresponsive-login-button`
 - `refactor/103-redesign-mitheme-engine`
 - `partner/aarhus/104-implement-basic-theme`
 - `partner/aarhus/bugfix/105-crash-in-aarhus-specific-motion-module`

The idea is to **give a clear, highly-focused purpose to each branch** and group them into distinctive sections.

```
IMPORTANT!!!: Always work on only 1 task at a time (on the given branch). 
If there is urgent hotfix, change related to other task, 
or you discovered that something else must be corrected 
and absolutely can't wait then: 
* commit the changes 
* create dedicated branch or switch if branch for that task already exists.
```

All feature branches must be pushed to the central repository. This makes it possible to share a feature with other developers without touching any official code. Since `dev` is the only “special” branch, storing several feature branches on the central repository doesn’t pose any problems. Of course, this is also a convenient way to back up everybody’s local commits.


## Branch naming conventions


### Main development line / Life app branches

All branch names for the work done on the shared code must be created using following patterns:

1. `feature/<ISSUE_ID-issue-title>` - for new features planned to be **shared across all partners** or Life app. You will see below, that every partner mjst have its own branch prefix but  **Life app is an exception** as it is a *default* FF application and belongs to the main `dev` branch.

1.  `bugfix/<ISSUE_ID-issue-title>` - for bugfixes discovered in the code shared across the partners.

3. `refactor/<ISSUE_ID-issue-title>` - for tasks related to refactoring  or redesigning the **shared code** or anything related to the FF project structure.

4. `hotfix/<ISSUE_ID-issue-title>` - for quick, urgent and fatal fixes done to the **shared code**. Please avoid as much as possible. Pull requests from this branch will be treated with special care.

5. `maintenance/<ISSUE_ID-issue-title>` - for maintenance related tasks, such as creating templates, restructuring folders, upgrading or creating cocoapods.

6. `documentation/<ISSUE_ID-issue-title>` - for documentation related tasks (please use [markdown bitbucket flavour](https://bitbucket.org/tutorials/markdowndemo) documents).


### Partner specific branching

Before work on the partner (client specific app) is started, so called `<partner_name>` must be decided upon. It is just the short, arbitrary code name chosen by developer from the client / app name. 

Partner codename should:

 - contain only alphanumeric characters, 
 - have no spaces, 
 - be lowercase,
 - have 'life' postfix removed, which often occurs in app names.

Few examples for the `<partner_name>` for apps:

 - *Aarhus Life*: `aarhus`, main dev branch: `partner/aarhus/dev`
 - *Region Syddanmark Life*: `regionsyd`, main dev branch: `partner/regionsyd/dev`

 The reason for requirements above is that the same name should be used for both: creating branch names and for folder containing resources for the specific partner, used by Xcode (we all know how Xcode 'loves' spaces in the paths).

All branch names dedicated to the partner must be created using following patterns:

1. `partner/<partner_name>/dev` - when you create a new partner app or continue working on the update. This will serve as an entry point to the changes requries by this specific app. You must further branch out from this branch to be able to work on specific tasks related to that parther.

2. `partner/<partner_name>/feature/<ISSUE_ID-issue-title>` - new features or code dedicated to **specific partner only** or basic, partner specific resource updates. 

3. `partner/<partner_name>/bugfix/<ISSUE_ID-issue-title>` - bugfixes where it  is 100% confidence that they appear in the code used in the specific partner only.

4. `partner/<partner_name>/refactor/<ISSUE_ID-issue-title>` - refactoring code found in the specific partner only.

5. `partner/<partner_name>/hotfix/<ISSUE_ID-issue-title>` - hotfixing code in files used by this partner only.
 
As you can see, this naming strategy is very similar to the main line [branch naming conventions](#markdown-header-partner-specific-branching).  
 
### Release branches

Every app release should be marked by the dedicated branch:

`release/partner/<partner_name>/<version_number` 

This is the case when releasing the partner app either using Enterpise or when uploaded to AppStore. This concerns **Life** app as well, ex:
`release/partner/life/<version_number`


## Pull requests

### Introduction:

Once the work on the feature is done, changes must not be immediately merged into `dev` (or `partner/<partner_name>/dev`). Instead, developers push the feature branch to the central server and file a pull request asking to merge their additions into main line. This gives other developers an opportunity to review the changes before they become a part of the main codebase.

Code review is a major benefit of pull requests, but they’re actually designed to be a generic way to talk about code. You can think of pull requests as a discussion dedicated to a particular branch. This means that they can also be used much earlier in the development process. For example, if a developer needs help with a particular feature, all they have to do is file a pull request. Interested parties will be notified automatically, and they’ll be able to see the question right next to the relevant commits.

### Process:

Everything to be merged to the `dev` (or `partner/<partner_name>/dev`) branch must be reviewed (by [lm@mobile-identity](mailto:lm@mobile-identity)). Schedule pull request when you decide that the bugfix, feature or partner branch is ready to be merged and reviewed.

Before creating pull request make sure that:

1. Your branch is up to date with the target branch (merge the other way around if necessary and resolve potential conflicts).
2. Code follows ["Mobile Identity official Objective-C style guide"](https://bitbucket.org/mobileidentity/objective-c-code-style-guide).
3. Project compiles.
4. All Unit Tests passes.
5. Typos... you know ;-)
6. ... and only `partner/<partner_name>/dev` branch can merged into `dev`.

Only when pull request is approved, developer should merge the branch into  target branch by himself (or use automated BitBucket or JIRA tool, if available) and push the updated branch back to the central repository.

---
### Workflow in short:

1. Create new branch giving it [proper name](#markdown-header-branch-naming-conventions)
2. Make changes
3. Prepare for pull request by:
	- merging all potential changes **from** the branch you will merge into, to make source branch up to date.
	- resolving all potential merge conflicts
	- making sure the code follows [style guide](https://bitbucket.org/mobileidentity/objective-c-code-style-guide)
	- project copiles
	- test are green
4. Create [pull requst] (#markdown-header-pull-requests)
5. Correct all changes suggested by reviewer
6. Merge into `dev` (or `partner/<partner_name>/dev`)
7. If merge was done into `partner/<partner_name>/dev` branch, prepare and make new pull request from that branch into `dev` branch starting from **step 3.**

---

## Other notes

Best if all tasks are created in JIRA project management system to support developers with managing source code. BitBucket issues are also an alternative until JIRA is up.

Best if all branch names are automatically created by JIRA project management system (branch name generated from task tape / task title and include issue number). Until then, just follow [branch naming conventions](#markdown-header-branch-naming-conventions) when manually specifying branch name.

## Say NO to submodules
It has been proven by painful experience how unpleasant and destructive are git submodules when it comes down to branching and keeping git workflows smooth. After way too long time of suffering they were thrown away and **must never go back.**
 
If any 3rd party librabry must be used, please add it using [CocoaPods](https://cocoapods.org), create the pod if does not exist or just include source files if absolutely necessary.


## Summary
#### Version: 
1.0.1
#### Author: 
Lukasz Marcin Margielewski [(lm@mobile-identity.com)](mailto:lm@mobile-identity)